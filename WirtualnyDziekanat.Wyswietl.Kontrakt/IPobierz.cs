﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Student.Kontrakt;



namespace WirtualnyDziekanat.Wyswietl.Kontrakt
{
    public interface IPobierz
    {
        ICollection<object> PobierzDane(int numerIndeksu);
        Dictionary<int, List<KeyValuePair<string, double>>> PobierzOceny(int numerIndeksu);
   
    }
}
