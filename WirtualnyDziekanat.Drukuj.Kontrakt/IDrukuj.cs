﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WirtualnyDziekanat.Drukuj.Kontrakt
{
    public interface IDrukuj
    {
        void DrukujNaPapier(Visual tresc);
        void DrukujDoPdf();
    }
}
