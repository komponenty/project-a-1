﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;
using WirtualnyDziekanat.Sterowanie;
using WirtualnyDziekanat.Wyswietlanie;
using WirtualnyDziekanat.ZarzadzanieDanymi;

namespace WirtualnyDziekanat.Infrastructure
{
    public class Config
    {
        public static IContainer Configure()
        {
            IContainer cont = new Container();
            cont.Register(Assembly.GetAssembly(typeof(Sterowanie.Sterowanie)));
            cont.Register(Assembly.GetAssembly(typeof(Wyswietlanie.Wyswietl)));
            cont.Register(Assembly.GetAssembly(typeof(ZarzadzanieDanymi.Dane)));
            cont.Register(Assembly.GetAssembly(typeof(WirtualnyDziekanat.DrukowanieNaPapier.DrukowanieNaPapier)));
            return cont;
        }
    }
}
