﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WirtualnyDziekanat.Pokaz.Kontrakt
{
    public interface IPokaz
    {
        UserControl PokazOceny(int numerIndeksu);
        UserControl PokazDane(int numerIndeksu);
    }
}
