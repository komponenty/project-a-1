﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using WirtualnyDziekanat.DrukowanieNaPapier.Kontrakt;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace WirtualnyDziekanat.DrukowanieNaPapier
{
    public class DrukowanieNaPapier : IDrukujNaPapier
    {
        public void Drukuj(Visual tresc)
        {

            PrintDialog dialog = new PrintDialog();
            if (dialog.ShowDialog() == true)
            {
                dialog.PrintVisual(tresc, "");
            }
        }

    }

}


