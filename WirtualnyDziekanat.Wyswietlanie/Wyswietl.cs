﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Wyswietl.Kontrakt;
using WirtualnyDziekanat.Student.Kontrakt;
using WirtualnyDziekanat.Pokaz.Kontrakt;



namespace WirtualnyDziekanat.Wyswietlanie
{
    public class Wyswietl : IPokaz
    {
        public IPobierz ip;
        public Wyswietl(IPobierz ip)
        {
            this.ip = ip;
        }

        private WyswietlS wyswietls;
        private WyswietlO wyswietlo;

        public System.Windows.Controls.UserControl PokazOceny(int numerIndeksu)
        {
            wyswietlo = new WyswietlO();
            var a = ip.PobierzOceny(numerIndeksu);
            if (a!=null)
            {
                wyswietlo.UstawOceny(a);
            }
            
            return wyswietlo;
        }

        public System.Windows.Controls.UserControl PokazDane(int numerIndeksu)
        {
            wyswietls = new WyswietlS();
            wyswietls.UstawDane(ip.PobierzDane(numerIndeksu));
            return wyswietls;
        }
    }
}
