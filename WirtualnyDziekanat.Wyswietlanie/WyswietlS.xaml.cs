﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WirtualnyDziekanat.Wyswietlanie
{
    /// <summary>
    /// Interaction logic for WyswietlS.xaml
    /// </summary>
    public partial class WyswietlS : UserControl
    {

        public WyswietlS()
        {

            InitializeComponent();
        }
        public void UstawDane(ICollection<object> dane)
        {
            List<object> lista = dane.ToList();
            tb1.Text = lista[0].ToString();
            tb2.Text = lista[1].ToString();
            tb3.Text = lista[2].ToString();
            tb4.Text = lista[3].ToString();
            tb5.Text = lista[4].ToString();
            tb6.Text = lista[5].ToString();
            tb7.Text = lista[6].ToString();
            tb8.Text = lista[7].ToString();
        }
    }
}
