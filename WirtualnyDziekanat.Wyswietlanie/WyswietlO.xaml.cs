﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WirtualnyDziekanat.Wyswietlanie
{
    /// <summary>
    /// Interaction logic for WyswietlO.xaml
    /// </summary>
    public partial class WyswietlO : UserControl
    {
        public WyswietlO()
        {
            InitializeComponent();
        }
        public void UstawOceny(Dictionary<int, List<KeyValuePair<string, double>>> Oceny)
        {
            foreach (var item in Oceny.Keys)
            {
                foreach (var item2 in Oceny[item])
                {
                    lb.Items.Add(item);
                    lb2.Items.Add(item2.Key);
                    lb3.Items.Add(item2.Value);
                }
            }

        }
    }
}
