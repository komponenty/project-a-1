﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WirtualnyDziekanat.Dane.Kontrakt
{
    public interface IDane
    {
        T PobierzObiekt<T>(T template);
        void DodawanieLubModyfikacja<T>(T student);
        void Usuń<T>(T student);
    }
}
