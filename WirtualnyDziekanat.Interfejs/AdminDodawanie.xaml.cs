﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Steruj.Kontrakt;
using System.Windows;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for AdminDodawanie.xaml
    /// </summary>
    public partial class AdminDodawanie : UserControl
    {
        ISteruj ist;
        public AdminDodawanie(ISteruj ist)
        {
            this.ist = ist;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] tab = tb5.Text.Split('.');
                ist.Stworz(new List<string> { tb1.Text, tb2.Text, tb3.Text, tb4.Text, tab[0], tab[1], tab[2], tb6.Text, tb7.Text, tb8.Text, null });
            }
            catch (Exception)
            {
                MessageBox.Show("Błędne dane");
            }
            
            clear();
        }
        private void clear()
        {
            tb1.Text = "";
            tb2.Text = "";
            tb3.Text = "";
            tb4.Text = "";
            tb5.Text = "";
            tb6.Text = "";
            tb7.Text = "";
            tb8.Text = "";
        }
    }
}
