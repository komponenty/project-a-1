﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace WirtualnyDziekanat.Interfejs
{
    class AdminWyswietlanieDanychVM : ViewModel
    {
        private Command cmd;
        public Command Cmd
        {
            get { return cmd; }
        }
        private string imie;
        public string Imie
        {
            get { return imie; }
            set
            {
                imie = value;
                FirePropertyChanged("imie");
            }
        }
        private string nazwisko;
        public string Nazwisko
        {
            get { return nazwisko; }
            set
            {
                nazwisko = value;
                FirePropertyChanged("nazwisko");
            }
        }
        private string numerIndeksu;
        public string NumerIndeksu
        {
            get { return numerIndeksu; }
            set
            {
                numerIndeksu = value;
                FirePropertyChanged("numerIndeksu");
            }
        }
        private string pesel;
        public string Pesel
        {
            get { return pesel; }
            set
            {
                pesel = value;
                FirePropertyChanged("pesel");
            }
        }
        private string dataUrodzenia;
        public string DataUrodzenia
        {
            get { return dataUrodzenia; }
            set
            {
                dataUrodzenia = value;
                FirePropertyChanged("dataUrodzenia");
            }
        }
        private string miejsceUrodzenia;
        public string MiejsceUrodzenia
        {
            get { return miejsceUrodzenia; }
            set
            {
                miejsceUrodzenia = value;
                FirePropertyChanged("miejsceUrodzenia");
            }
        }
        private string adres;
        public string Adres
        {
            get { return adres; }
            set
            {
                adres = value;
                FirePropertyChanged("adres");
            }
        }
        private string adresEmail;
        public string AdresEmail
        {
            get { return adresEmail; }
            set
            {
                adresEmail = value;
                FirePropertyChanged("adresEmail");
            }
        }
        public AdminWyswietlanieDanychVM(Command cmd)
        {
            this.cmd = cmd;
        }
    }
}
