﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Pokaz.Kontrakt;
using System.Windows.Controls;
using WirtualnyDziekanat.Steruj.Kontrakt;
using WirtualnyDziekanat.Drukuj.Kontrakt;
using TinyMVVMHelper;
using System.Windows;
using WirtualnyDziekanat.Wyswietl.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    class Interfejs
    {
        ISteruj ist;
        IDrukuj id;
        IPokaz ip;
        IPobierz ipo;
        MainWindow mw;
        StronaLogowania sl;
        StronaLogowaniaVM slVM;
        AdminPanel ap;
        AdminDodawanie ad;
        AdminUsunStudenta au;
        StudentPanel sp;
        StudentZmianaHasla sz;
        AdminWyswietlanieDanych aw;
        AdminWyswietlanieDanychVM awVM;
        AdminWyswietlanieOcen awo;
        AdminWyswietlanieOcenVM awoVM;
        AdminWybor awyb;
        AdminWyborVM awybVM;
        StudentDane sd;
        StudentOceny so;
        public Interfejs(ISteruj ist, IDrukuj id, IPokaz ip,IPobierz ipo, MainWindow mw)
        {
            this.ist = ist;
            this.id = id;
            this.ip = ip;
            this.ipo = ipo;
            this.mw = mw;
            sl = new StronaLogowania();
            slVM = new StronaLogowaniaVM(new Command(Login));
            sl.DataContext = slVM;
            ad = new AdminDodawanie(ist);
            au = new AdminUsunStudenta(ist);
            aw = new AdminWyswietlanieDanych(ist, ipo);
            awVM = new AdminWyswietlanieDanychVM(new Command(AktualizacjaDanych));
            aw.DataContext = awVM;
            awo = new AdminWyswietlanieOcen(ist, ipo);
            awoVM = new AdminWyswietlanieOcenVM(new Command(AktualizacjaOcen));
            awo.DataContext = awoVM;
            awyb = new AdminWybor();
            awybVM = new AdminWyborVM(new Command(Dane), new Command(Oceny));
            awyb.DataContext = awybVM;
            ap = new AdminPanel(mw, ad, awyb, au);
            sz = new StudentZmianaHasla(ist);
            sd = new StudentDane(id);
            so = new StudentOceny(id);          
            sp = new StudentPanel(mw, sz, sd, so,ip);
            mw.MainGrid.Children.Add(sl);
            mw.Show();
        }
        private void Login(object paramater)
        {
            int a;
            string login = slVM.Login;
            string haslo = slVM.Pass;
            if (login == "admin" && haslo == "admin")
            {
                sl.Visibility = Visibility.Hidden;
                mw.panelGrid.Children.Clear();
                mw.panelGrid.Children.Add(ap);

            }
            else
            {
                if (int.TryParse(login, out a))
                {
                    if (ist.Zaloguj(a, haslo))
                    {
                        sl.Visibility = Visibility.Hidden;
                        mw.panelGrid.Children.Clear();
                        mw.panelGrid.Children.Add(sp);
                    }
                    else
                    {
                        MessageBox.Show("Błędny login lub haslo");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Błędny login lub haslo");
                }
                
            }
        }
        private void Dane(object parameter)
        {
            int a;
            if (int.TryParse(awybVM.Numer,out a))
            {
                if (aw.Ustaw(a))
                {
                    mw.UstawOkno(aw);
                }
            }
        }
        private void Oceny(object parameter)
        {
            int a;
            if (int.TryParse(awybVM.Numer, out a))
            {
                if (awo.Ustaw(a))
                {
                    mw.UstawOkno(awo);
                }                
            }
        }
        private void AktualizacjaOcen(object parameter)
        {
            try
            {
            ((ISteruj) ipo).DodajOcene(int.Parse(awoVM.Semestr), awoVM.Przedmiot, double.Parse(awoVM.Ocena));
            Oceny(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Błędne dane");
            }

            awoVM.Semestr = "";
            awoVM.Przedmiot = "";
            awoVM.Ocena = "";
        }
        private void AktualizacjaDanych(object parameter)
        {
            try
            {
                string[] tab = awVM.DataUrodzenia.Split('-');
                ist.Aktualizuj(new List<string> { awVM.Imie, awVM.Nazwisko, awVM.NumerIndeksu, awVM.Pesel, tab[0], tab[1], tab[2],
                    awVM.MiejsceUrodzenia, awVM.Adres, awVM.AdresEmail, null });
                Dane(null);
            }
            catch (Exception)
            {

            }
        }
    }
}
