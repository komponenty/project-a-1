﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Steruj.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for AdminUsunStudenta.xaml
    /// </summary>
    public partial class AdminUsunStudenta : UserControl
    {
        ISteruj ist;
        public AdminUsunStudenta(ISteruj ist)
        {
            this.ist = ist;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int a;
            if (int.TryParse(this.tb.Text,out a))
            {
                ist.Zniszcz(a);
            }
            tb.Text = "";
        }
    }
}
