﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WirtualnyDziekanat.Infrastructure;
using WirtualnyDziekanat.ZarzadzanieDanymi;
using WirtualnyDziekanat.Wyswietlanie;
using WirtualnyDziekanat.Sterowanie;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void AppStart(object sender, StartupEventArgs e)
        {
            var container = Config.Configure();
            //var dane = container.Resolve<Dane.Kontrakt.IDane>();
            var wyswietlanie = container.Resolve<Pokaz.Kontrakt.IPokaz>();
            var sterowanie = container.Resolve<Steruj.Kontrakt.ISteruj>();
            var drukowanie = container.Resolve<Drukuj.Kontrakt.IDrukuj>();
            var pobierz = container.Resolve<Wyswietl.Kontrakt.IPobierz>();
            MainWindow mw = new MainWindow();
            (wyswietlanie as WirtualnyDziekanat.Wyswietlanie.Wyswietl).ip = (sterowanie as WirtualnyDziekanat.Wyswietl.Kontrakt.IPobierz);
            Interfejs i = new Interfejs(sterowanie, drukowanie, wyswietlanie,pobierz, mw);
            
        }
    }
}
