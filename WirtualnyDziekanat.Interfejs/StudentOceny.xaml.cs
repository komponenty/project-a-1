﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Drukuj.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for StudentOceny.xaml
    /// </summary>
    public partial class StudentOceny : UserControl
    {
        IDrukuj idr;
        UserControl u;
        public StudentOceny(IDrukuj idr)
        {
            this.idr = idr;
            InitializeComponent();
        }
        public void SetGrid(UserControl uc)
        {
            this.u = uc;
            this.g.Children.Clear();
            this.g.Children.Add(uc);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            idr.DrukujNaPapier(u);
        }
    }
}
