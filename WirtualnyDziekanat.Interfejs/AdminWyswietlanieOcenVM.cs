﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace WirtualnyDziekanat.Interfejs
{
    class AdminWyswietlanieOcenVM : ViewModel
    {
        private Command cmd;
        public Command Cmd
        {
            get { return cmd; }
        }
        private string semestr;
        public string Semestr
        {
            get { return semestr; }
            set
            {
                semestr = value;
                FirePropertyChanged("semestr");
            }
        }
        private string przedmiot;
        public string Przedmiot
        {
            get { return przedmiot; }
            set
            {
                przedmiot = value;
                FirePropertyChanged("przedmiot");
            }
        }
        private string ocena;
        public string Ocena
        {
            get { return ocena; }
            set
            {
                ocena = value;
                FirePropertyChanged("ocena");
            }
        }
        public AdminWyswietlanieOcenVM(Command cmd)
        {
            this.cmd = cmd;
        }
    }
}
