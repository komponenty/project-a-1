﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace WirtualnyDziekanat.Interfejs
{
    class StronaLogowaniaVM : ViewModel
    {
        private Command cmd;
        public Command Cmd
        {
            get { return cmd; }
        }
        public StronaLogowaniaVM(Command cmd)
        {
            this.cmd = cmd;
        }
        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                FirePropertyChanged("login");
            }
        }
        private string pass;
        public string Pass
        {
            get { return pass; }
            set
            {
                pass = value;
                FirePropertyChanged("pass");
            }
        }
    }
}
