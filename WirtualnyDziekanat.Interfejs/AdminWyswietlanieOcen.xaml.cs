﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Steruj.Kontrakt;
using WirtualnyDziekanat.Wyswietl.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for AdminWyswietlanieOcen.xaml
    /// </summary>
    public partial class AdminWyswietlanieOcen : UserControl
    {
        ISteruj ist;
        IPobierz ipo;
        public AdminWyswietlanieOcen(ISteruj ist, IPobierz ipo)
        {
            this.ist = ist;
            this.ipo = ipo;
            this.ist = (ISteruj)ipo;
            InitializeComponent();
        }


        public bool Ustaw(int numerIndeksu)
        {
            lb.Items.Clear();
            lb2.Items.Clear();
            lb3.Items.Clear();
            var a = ipo.PobierzOceny(numerIndeksu);
            if (a != null)
            {
                foreach (var item in a.Keys)
                {
                    foreach (var item2 in a[item])
                    {
                        lb.Items.Add(item);
                        lb2.Items.Add(item2.Key);
                        lb3.Items.Add(item2.Value);
                    }
                }
                return true;
            }
            return false;

        }
    }
}
