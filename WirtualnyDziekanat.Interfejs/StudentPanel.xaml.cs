﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Pokaz.Kontrakt;
using WirtualnyDziekanat.Steruj.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for StudentPanel.xaml
    /// </summary>
    public partial class StudentPanel : UserControl
    {
        MainWindow mw;
        UserControl zmi;
        StudentDane dane;
        StudentOceny oceny;
        IPokaz ip;
        public StudentPanel(MainWindow mw, UserControl zmi, StudentDane dane, StudentOceny oceny, IPokaz ip)
        {
            this.ip = ip;
            this.dane = dane;
            this.oceny = oceny;
            this.mw = mw;
            this.zmi = zmi;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dane.SetGrid(ip.PokazDane(0));
            mw.UstawOkno(dane);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            oceny.SetGrid(ip.PokazOceny(0));
            mw.UstawOkno(oceny);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            mw.UstawOkno(zmi);
        }
    }
}
