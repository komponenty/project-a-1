﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace WirtualnyDziekanat.Interfejs
{
    class AdminWyborVM : ViewModel
    {
        private Command dane;
        public Command Dane
        {
            get { return dane; }
        }
        private Command oceny;
        public Command Oceny
        {
            get { return oceny; }
        }
        private string numer;
        public string Numer
        {
            get { return numer; }
            set
            {
                numer = value;
                FirePropertyChanged("login");
            }
        }
        public AdminWyborVM(Command dane, Command oceny)
        {
            this.dane = dane;
            this.oceny = oceny;
        }
    }
}
