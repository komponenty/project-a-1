﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for AdminPanel.xaml
    /// </summary>
    public partial class AdminPanel : UserControl
    {
        MainWindow w;
        UserControl dod;
        UserControl wyb;
        UserControl usu;
        public AdminPanel(MainWindow w, UserControl dod, UserControl wyb, UserControl usu)
        {
            this.dod = dod;
            this.wyb = wyb;
            this.usu = usu;
            this.w = w;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            w.UstawOkno(dod);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            w.UstawOkno(usu);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            w.UstawOkno(wyb);
        }
    }
}
