﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WirtualnyDziekanat.Steruj.Kontrakt;
using WirtualnyDziekanat.Wyswietl.Kontrakt;

namespace WirtualnyDziekanat.Interfejs
{
    /// <summary>
    /// Interaction logic for AdminWyswietlanie.xaml
    /// </summary>
    public partial class AdminWyswietlanieDanych : UserControl
    {
        ISteruj ist;
        IPobierz ipo;
        public AdminWyswietlanieDanych(ISteruj ist, IPobierz ipo)
        {
            this.ist = ist;
            this.ipo = ipo;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] tab = tb5.Text.Split('-');
                ist.Aktualizuj(new List<string> { tb1.Text, tb2.Text, tb3.Text, tb4.Text, tab[0], tab[1], tab[2], tb6.Text, tb7.Text, tb8.Text, null });
            }
            catch (Exception)
            {
                
            }
            
        }
        public bool Ustaw(int numerIndeksu)
        {
            List<object> lista = ipo.PobierzDane(numerIndeksu).ToList();
            if (lista.Count == 9)
            {
                tb1.Text = lista[0].ToString();
                tb2.Text = lista[1].ToString();
                tb3.Text = lista[2].ToString();
                tb4.Text = lista[3].ToString();
                tb5.Text = ((DateTime)lista[4]).ToShortDateString();
                tb6.Text = lista[5].ToString();
                tb7.Text = lista[6].ToString();
                tb8.Text = lista[7].ToString();
                return true;
            }
            return false;
        }
    }
}
