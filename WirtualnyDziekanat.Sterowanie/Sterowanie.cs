﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Dane.Kontrakt;
using WirtualnyDziekanat.Steruj.Kontrakt;
using WirtualnyDziekanat.Wyswietl.Kontrakt;
using WirtualnyDziekanat.Drukuj.Kontrakt;
using WirtualnyDziekanat.DrukowanieNaPapier.Kontrakt;

namespace WirtualnyDziekanat.Sterowanie
{
    public class Sterowanie : ISteruj, IDrukuj, IPobierz
    {
        private Student aktualnyStudent;
        private IDane dane;
        private IDrukujNaPapier idr;
        public Sterowanie(IDane dane, IDrukujNaPapier idr)
        {
            this.dane = dane;
            this.idr = idr;
        }

        public void Pobierz(int numerIndeksu)
        {
            aktualnyStudent = dane.PobierzObiekt<Student>(new Student(null, null, numerIndeksu, 0, new DateTime(), null, null, null));
        }

        public void Stworz(IEnumerable<string> argumenty)
        {
            List<string> l = argumenty.ToList<string>();
            try
            {
                Student student = new Student(l[0], l[1], int.Parse(l[2]), int.Parse(l[3]),
                new DateTime(int.Parse(l[4]), int.Parse(l[5]), int.Parse(l[6])), l[7], l[8], l[9]);

            student.GeneracjaHasla();
            dane.DodawanieLubModyfikacja<Student>(student);

            }
            catch (Exception)
            {
            }
            
        }

        public void Zniszcz(int numerIndeksu)
        {
            dane.Usuń<Student>(new Student(null, null, numerIndeksu, 0, new DateTime(), null, null, null));
        }

        public bool Zaloguj(int login, string haslo)
        {
            Student s = new Student(null, null, login, 0, new DateTime(), null, null, null);
            s.Haslo = null;
            aktualnyStudent = dane.PobierzObiekt<Student>(s);
            if (aktualnyStudent == null)
            {
                return false;
            }
            string przyznaneHasło = aktualnyStudent.Haslo;
            if (przyznaneHasło == haslo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public void DrukujDoPdf()
        {
            throw new NotImplementedException();
        }


        public ICollection<object> PobierzDane(int numerIndeksu)
        {
            if (numerIndeksu != 0)
            {
                Pobierz(numerIndeksu);
            }
            if (aktualnyStudent!=null)
            {
                return aktualnyStudent.ZwrocDane();
            }
            return new List<object>();
        }

        public Dictionary<int, List<KeyValuePair<string, double>>> PobierzOceny(int numerIndeksu)
        {
            if (aktualnyStudent == null)
            {
                Pobierz(numerIndeksu);
            }
            if (numerIndeksu != 0 )
            {
                Pobierz(numerIndeksu);
            }
            if (aktualnyStudent == null)
            {
                return null;
            }
            return aktualnyStudent.ZwrocOceny();
        }


        public void ZmianaHasla(string haslo)
        {
            int i = aktualnyStudent.Numer_indeksu;
            Zniszcz(i);

            aktualnyStudent.Haslo = haslo;
            dane.DodawanieLubModyfikacja<Student>(aktualnyStudent);
        }
        public void Aktualizuj()
        {
            int i = aktualnyStudent.Numer_indeksu;
            Zniszcz(i);
            dane.DodawanieLubModyfikacja<Student>(aktualnyStudent);
        }


        public void DodajOcene(int semestr, string przedmiot, double ocena)
        {
            this.aktualnyStudent.Oceny.DodajZmienOcene(semestr,przedmiot,ocena);
            Aktualizuj();
        }


        public void Aktualizuj(IEnumerable<string> argumenty)
        {
            
            List<string> l = argumenty.ToList<string>();
            
            try
            {
                Pobierz(int.Parse(l[2]));
                Student s = aktualnyStudent;
                Zniszcz(int.Parse(l[2]));
                Student student = new Student(l[0], l[1], int.Parse(l[2]), int.Parse(l[3]),
                new DateTime(int.Parse(l[4]), int.Parse(l[5]), int.Parse(l[6])), l[7], l[8], l[9]);
                student.Haslo = aktualnyStudent.Haslo;
                student.UstawOceny(aktualnyStudent.Oceny);
                dane.DodawanieLubModyfikacja<Student>(student);
            }
            catch (Exception)
            {
            }
        }

        public void DrukujNaPapier(System.Windows.Media.Visual tresc)
        {
            idr.Drukuj(tresc);
        }
    }
}
