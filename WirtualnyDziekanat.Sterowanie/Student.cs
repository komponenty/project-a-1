﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Student.Kontrakt;


namespace WirtualnyDziekanat.Sterowanie
{
    class Student : IStudent
    {
        //Dane studenta
        private string imie;
        public String Imie
        {
            get { return imie; }
        }

        private string nazwisko;
        public string Nazwisko
        {
            get { return nazwisko; }
        }

        private int numer_indeksu;
        public int Numer_indeksu
        {
            get { return numer_indeksu; }
        }

        private int pesel;
        public int Pesel
        {
            get { return pesel; }
        }

        private DateTime data_urodzenia;
        public DateTime Data_urodzenia
        {
            get { return data_urodzenia; }
        }

        private string miejsce_urodzenia;
        public string Miejsce_urodzenia
        {
            get { return miejsce_urodzenia; }
        }

        private string adres;
        public string Adres
        {
            get { return adres; }
        }

        private string email;
        public string Email
        {
            get { return email; }
        }

        private string haslo;
        public string Haslo
        {
            get { return haslo; }
            set { haslo = value; }
        }

        private Indeks oceny = new Indeks();
        public Indeks Oceny
        {
            get { return oceny; }
        }
        
        //Konstruktor klasy Student
        public Student(string imie, string nazwisko, int numer_indeksu, int pesel, DateTime data_urodzenia, string miejsce_urodzenia, string adres, string email)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.numer_indeksu = numer_indeksu;
            this.pesel = pesel;
            this.data_urodzenia = data_urodzenia;
            this.miejsce_urodzenia = miejsce_urodzenia;
            this.adres = adres;
            this.email = email;
        }
        //Metoda generująca automatycznie hasło
        public void GeneracjaHasla()
        {
            Random r = new Random();
            string haslo = "";
            foreach (var item in Enumerable.Range(1,8))
	        {
               haslo+= r.Next(0,9).ToString();
	        }
            this.haslo = haslo;
        }
        //Implementacja metody z interfejsu IStudent, zwracajaca dane studenta
        public ICollection<object> ZwrocDane()
        {
            List<object> lista = new List<object>
            {
                imie,
                nazwisko,
                numer_indeksu,
                pesel,
                data_urodzenia,
                miejsce_urodzenia,
                adres,
                email,
                haslo
            };

            return lista;
        }

        //Implementacja metody z interfejsu IStudent, zwracajaca oceny studenta
        public Dictionary<int, List<KeyValuePair<string, double>>> ZwrocOceny()
        {
            if (this.oceny != null)
            {
                return oceny.Oceny;
            }
            return null;
        }

        //Implementacja metody z interfejsu IStudent, zwracająca średnią ocen studenta z danego semestru
        public double Srednia(int semestr)
        {
            return oceny.WyliczSrednia(semestr);
        }
        public void UstawOceny(Indeks o)
        {
            this.oceny = o;
        }
    }
}

