﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WirtualnyDziekanat.Sterowanie
{
    class Indeks
    {
        private Dictionary<int, List<KeyValuePair<string, double>>> oceny = new Dictionary<int, List<KeyValuePair<string, double>>>();
        public Dictionary<int,List<KeyValuePair<string, double>>> Oceny
        {
            get { return oceny; }
        }

        //Metoda znajdująca czy dany semestr zawiera podany przedmiot
        private int Znajdz(int semestr, string przedmiot)
        {
            if (!oceny.ContainsKey(semestr))
            {
                return -1;
            }

            for (int i = 0; i < oceny[semestr].Count; i++)
            {
                if (oceny[semestr][i].Key == przedmiot)
                {
                    return i;
                }
            }
            return -1;
        }

        //Metoda umożliwiająca dodanie lub modyfikowanie ocen
        public void DodajZmienOcene(int semestr, string przedmiot, double ocena)
        {
            int z = Znajdz(semestr, przedmiot);
            if (!oceny.ContainsKey(semestr))
            {
                oceny.Add(semestr, new List<KeyValuePair<string, double>>());
            }
            if (z == -1)
            {
                oceny[semestr].Add(new KeyValuePair<string, double>(przedmiot, ocena));
            }
            else
            {
                oceny[semestr][z] = new KeyValuePair<string, double>(przedmiot, ocena);
            }
        }

        public double WyliczSrednia(int semestr)
        {
            int liczbaPrzedmiotow = oceny[semestr].Count;
            double suma = 0.0;
            for (int i = 0; i < liczbaPrzedmiotow ; i++)
            {
                suma = suma + oceny[semestr][i].Value;
            }

            return suma / liczbaPrzedmiotow;
        }
    }
}
