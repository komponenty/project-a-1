﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WirtualnyDziekanat.Dane.Kontrakt;
using Db4objects.Db4o;
using Db4objects.Db4o.Config;

namespace WirtualnyDziekanat.ZarzadzanieDanymi
{
    public class Dane : IDane
    {
        private string sciezka = "database.yap";
        public T PobierzObiekt<T>(T template)
        {
            IEmbeddedConfiguration config = Db4oEmbedded.NewConfiguration();
            IObjectContainer db = Db4oEmbedded.OpenFile(config, sciezka);
            IObjectSet result =  db.QueryByExample(template);
            int a = result.Count;
            if (a != 0)
            {
                T s = (T)result[0];
                db.Close();
                return s;
            }
            db.Close();
            return default(T);
        }

        public void DodawanieLubModyfikacja<T>(T student)
        {
            IEmbeddedConfiguration config = Db4oEmbedded.NewConfiguration();
            IObjectContainer db = Db4oEmbedded.OpenFile(config, sciezka);
            IObjectSet result = db.QueryByExample(student);
            
            if (result.Count == 0)
            {
                db.Store(student);
            }
            else
            {
                result[0] = student;
                db.Store(result[0]);
            }
            db.Close();    
        }

        public void Usuń<T>(T student)
        {
            IEmbeddedConfiguration config = Db4oEmbedded.NewConfiguration();
            IObjectContainer db = Db4oEmbedded.OpenFile(config, sciezka);
            IObjectSet result = db.QueryByExample(student);
            if (result.Count != 0)
            {
                db.Delete(result[0]);
            }
            db.Close();
        }
    }
}
