﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WirtualnyDziekanat.Steruj.Kontrakt
{
    public interface ISteruj
    {
        void Stworz(IEnumerable<string> argumenty);
        void Zniszcz(int numerIndeksu);
        void ZmianaHasla(string haslo);
        bool Zaloguj(int login, string haslo);
        void DodajOcene(int semestr, string przedmiot, double ocena);
        void Aktualizuj(IEnumerable<string> argumenty);
    }
}
