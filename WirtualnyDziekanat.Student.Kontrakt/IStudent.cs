﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WirtualnyDziekanat.Student.Kontrakt
{
    public interface IStudent
    {
        ICollection<object> ZwrocDane();
        Dictionary<int, List<KeyValuePair<string, double>>> ZwrocOceny();
        double Srednia(int semestr);
    }
}
