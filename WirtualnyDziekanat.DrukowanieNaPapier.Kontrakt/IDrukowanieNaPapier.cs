﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WirtualnyDziekanat.DrukowanieNaPapier.Kontrakt
{
    public interface IDrukujNaPapier
    {
		void Drukuj(Visual tresc);
    }
}
